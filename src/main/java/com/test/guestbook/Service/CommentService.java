package com.test.guestbook.Service;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.test.guestbook.entity.Comment;
import com.test.guestbook.DAO.CommentRepository;
@Service
public class CommentService implements ICommentService{
    private CommentRepository commentRepository;

    @Autowired
    public CommentService(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Override
    public List<Comment> getAllComments() {
        return commentRepository.findAll();
    }
    @Override
    public Comment getCommentById(Integer commentId) {
        Comment comm = commentRepository.getOne(commentId);
        return comm;
    }
    @Override
    public synchronized void addComment(Comment comm) {
        commentRepository.save(comm);
    }
    public void deleteComment(Integer id) {
        commentRepository.delete(id);
    }

}

package com.test.guestbook.Service;
import java.util.List;
import com.test.guestbook.entity.Comment;

public interface ICommentService {
    List<Comment> getAllComments();
    void addComment(Comment comm);
    void deleteComment(Integer id);
    Comment getCommentById(Integer commentId);
}

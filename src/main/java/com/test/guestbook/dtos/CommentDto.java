package com.test.guestbook.dtos;

import com.test.guestbook.entity.Comment;

import java.time.LocalDateTime;

public class CommentDto {
    private Integer id;
    private String title;
    private String text;
    private LocalDateTime created;

    public CommentDto(Comment comment) {
        id = comment.getCommentId();
        title = comment.getTitle();
        text = comment.getText();
        created = comment.getCreated();
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public LocalDateTime getCreated() {
        return created;
    }
}

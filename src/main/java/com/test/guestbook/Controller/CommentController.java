package com.test.guestbook.Controller;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.test.guestbook.dtos.CommentDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.test.guestbook.Service.ICommentService;
import com.test.guestbook.entity.Comment;

@RestController
@RequestMapping("comments")
public class CommentController {
    public static class TempDto {
        public String title;
        public String text;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }


    private ICommentService commentService;

    @Autowired
    public CommentController(ICommentService commentService) {
        this.commentService = commentService;
    }
    @GetMapping
    public @ResponseBody List<CommentDto> getComments() {
        List<Comment> lst = commentService.getAllComments();
        List<CommentDto> result = new ArrayList<CommentDto>();
        for(Comment comm: lst) {
            CommentDto tmp = new CommentDto(comm);
            result.add(tmp);
        }
        return result;
    }
    @PostMapping
    public void addComment(@RequestBody TempDto comm) {
        Comment tmp = new Comment();
        tmp.setTitle(comm.getTitle());
        tmp.setText(comm.getText());
        tmp.setCreated(LocalDateTime.now());
        commentService.addComment(tmp);
    }
    @DeleteMapping
    public void deleteComment(@RequestBody Integer id) {
        commentService.deleteComment(id);
    }
}


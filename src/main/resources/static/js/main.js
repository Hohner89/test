/**
 * Created by work on 14.08.17.
 */
(function () {
    $.ajaxSetup({
        beforeSend: function(jqXHR,options){
            if ( options.contentType === "application/json" && typeof options.data !== "string" ) {
                options.data = JSON.stringify(options.data);
            }
        }
    });
}) ();
function funcOutput(data) {
    $.each(data, function(){
        $('#comments').append('<h2 class="',data.id,'">',data.title,'</h2>');
        $('#comments').append('<p class="',data.id,'">',data.created,'</p>');
        $('#comments').append('<p class="',data.id,'">',data.text,'</p>');
    });
}
function funcErr(data) {
    console.log(data);
}
(function () {
    $.ajax({
        type:'GET',
        url:'/comments',
        dataType: 'json',
        success: funcOutput,
        error: funcErr
    });
}) ();

